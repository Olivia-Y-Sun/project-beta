import React, { useState, useEffect } from "react";

export const CustomerForm = () => {
    const [customer, setCustomer] = useState({
        name: '',
        address: '',
        phone: ''
    })

    const handleInputChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        setCustomer({ ...customer, [name]: value });
      };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {...customer };

        await fetch("http://localhost:8090/api/customers/", {
            method: "POST",
            body: JSON.stringify(data),
            headers: {"Content-Type": "application/json"}
        }).then((e) => {
            window.location.href = "/sales"
        })
    }


  return (
    <div className='container-fluid'>
      <div className='row'>
        <div className='offset-3 col-6'>
          <div className='shadow p-4 mt-4'>
            <h1>Add a Potential Customer</h1>

            <form onSubmit={handleSubmit} className='form'>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={customer.name}
                  placeholder='Name'
                  required
                  type='text'
                  name='name'
                  id='name'
                  className='form-control'
                />
                <label htmlFor='name'>Name</label>
              </div>
              <div className='mb-3'>
                <textarea
                  onChange={handleInputChange}
                  value={customer.address}
                  placeholder='Address'
                  required
                  type='text'
                  name='address'
                  rows="3"
                  id='address'
                  className='form-control'
                ></textarea>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={customer.phone}
                  placeholder='Phone Number'
                  required
                  type='text'
                  name='phone'
                  id='phone'
                  className='form-control'
                />
                <label htmlFor='phone'>Phone Number</label>
              </div>
              <div className='d-grid gap-2 d-md-flex justify-content-md-end'>
                <button className='btn btn-outline-success'>Add</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}
