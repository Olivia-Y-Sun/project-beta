import react, {useState, useEffect} from 'react';
import {Link, NavLink} from 'react-router-dom';

const SalesList = () => {
    const [loading, setLoading] = useState(false);
    const [sales, setSales] = useState([]);
    const url = "http://localhost:8090/api/sales/";

    const fetchData = async () => {
        try {
            const response = await fetch(url);
            if (response.ok) {
                const theJson = await response.json();


                setLoading(false)
                setSales(theJson.sales)
            }
        } catch (e) {
            console.log("error", e)
        }
    };

    useEffect(() => {
        setLoading(true);
        fetchData();
    }, [])

    return (
        <div className='container-fluid p-2'>
            <div className='d-grid gap-2 d-md-flex justify-content-md-center'>
            <Link className="btn btn-outline-success g-2" to="/sales/new">Create a Sale</Link>
            <Link className="btn btn-outline-success g-2" to="/employees/new">Add a Sales Employee</Link>
            <Link className="btn btn-outline-success g-2" to="/customers/new">Add a Customer</Link>
            </div>
            <div className='d-grid gap-2 d-md-flex justify-content-md-center p-2'>
                <Link className="btn btn-outline-success g-2" to="/employees/sales">List Sales by Employee</Link>
            </div>
            {loading &&(
                <div>
                    {" "}
                    <h1>Loading...</h1>
                </div>
            )}
            <h1>Sales Records</h1>
            <table className="table table-striped">
            <thead>
                <tr>
                <th>Employee ID</th>
                <th>Employee Name</th>
                <th>Customer</th>
                <th>Vin #</th>
                <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {sales.map((sale) => {
                return (
                    <tr key={sale.href}>
                    <td>{sale.sales_person.employee_number}</td>
                    <td>{sale.sales_person.name}</td>
                    <td>{sale.customer.name}</td>
                    <td>{sale.automobile.vin}</td>
                    <td>{"$" + sale.price}</td>
                    </tr>
                );
                })}
            </tbody>
            </table>
        </div>
    );
}

export default SalesList;
