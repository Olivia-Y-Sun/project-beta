import React, { useState, useEffect } from "react";

const ServiceSearch = () => {
  const [services, setService] = useState([]);
  const [autos, setAutos] = useState([]);
  const [filterService, setfilterService] = useState([]);
  const [searchInput, setSearchInput] = useState("");
  const url = "http://localhost:8080/api/services/";

  const handleInputChange = (e) => {
    const value = e.target.value;
    setSearchInput( value );
  };


  const fetchService = async () => {
    try {
      const response = await fetch(url);
      const content = await response.json();
      if (response.ok) {
        const filterServices = content.services.filter(
          (service) => service.is_finished === true
        );
        setService(filterServices);
        setfilterService(filterServices);
      }
    } catch (e) {
      console.log("error", e);
    }
  };

  const fetchAutomobiles = async () => {
    const url = "http://localhost:8100/api/automobiles/"
    try {
        const response = await fetch(url);

        if (response.ok) {
            const content = await response.json();
            console.log(content.autos);
            setAutos((a) => ({...a, automobiles: content.autos}));
        }
    } catch (e) {
        console.log("error", e);
    }
  }


  useEffect(() => {
    fetchService();
    fetchAutomobiles();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (searchInput) {
      const filterService = services.filter(
        (service) => service.vin === searchInput
      );
      setfilterService(filterService);
    } else {
      setfilterService(services);
    }

  };

  function myalert() {
    alert("This vin number does not exist.");
  }

  if (autos === []) {
    return (myalert)
  } else {
    return (
      <div className='container-fluid'>

        <form onSubmit={handleSubmit} className='form'>
          <div className='form-floating mb-3'>

            <input
              onChange={handleInputChange}
              value={services.vin}
              placeholder='vin'
              // required
              type='text'
              name='vin'
              id='vin'
              className='form-control'
            />
            <label htmlFor='vin'>vin</label>
            <button className='btn btn-outline-success'>Search VIN</button>
          </div>
        </form>

        <h1>Service History</h1>
        <table className='table table-striped'>
          <thead>
            <tr>
              <th>Customer Name</th>
              <th>Vin</th>
              <th>Technician</th>
              <th>Date</th>
              <th>Time</th>
              <th>Reason</th>
            </tr>
          </thead>
          <tbody>
            {console.log(services)}
            {filterService.map((service) => (
              <tr key={service.id}>
                <td>{service.customer_name}</td>
                <td>{service.vin}</td>
                <td>{service.technician.name}</td>
                <td>{service.service_date}</td>
                <td>{service.service_time}</td>
                <td>{service.reason}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
};


export default ServiceSearch;
