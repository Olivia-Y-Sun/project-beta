import react, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';

export const VehicleModelList = () => {
    const [loading, setLoading] = useState(false);
    const [vehicleModels, setVehicleModels] = useState([])
    const url = "http://localhost:8100/api/models/"

    const fetchData = async () => {
        try {
            const response = await fetch(url);
            if (response.ok) {
                const theJson = await response.json();

                setLoading(false)
                setVehicleModels(theJson.models)
            }
        } catch (e) {
            console.log("error", e)
        }
    };

    useEffect(() => {
        setLoading(true);
        fetchData();
      }, [])

    return (
        <div className='container-fluid p-2'>
            <div className='d-grid gap-2 d-md-flex justify-content-md-center'>
                <Link className="btn btn-outline-success g-2" to="/models/new">Create a Vehicle Model</Link>
            </div>
            {loading &&(
                <div>
                    {" "}
                    <h1>Loading...</h1>
                </div>
            )}
            <h1>Vehicle Models</h1>
            <table className="table table-striped ">
                <thead>
                    <tr className="row gx-5">
                    <th className='col'>Model</th>
                    <th className='col'>Manufacturer</th>
                    <th className='col'>Photo</th>
                    </tr>
                </thead>
                <tbody>
                    {vehicleModels.map((vehicleModel) => {
                    return (
                        <tr className="row gx-5" key={vehicleModel.href}>
                        <td className="col">{vehicleModel.name}</td>
                        <td className="col">{vehicleModel.manufacturer.name}</td>
                        <td className="col"><img src={vehicleModel.picture_url} className="img-thumbnail" alt="Sorry This is Not Available"/></td>
                        </tr>
                    );
                    })}
                </tbody>
            </table>
        </div>
    )
}
