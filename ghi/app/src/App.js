import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ServiceList from "./Components/ServiceList";
import ServiceCreate from "./Components/ServiceCreate";
import ServiceSearch from "./Components/ServiceSearch";
import TechnicianList from "./Components/TechnicianList"
import TechnicianCreate from "./Components/TechnicianCreate"
import SalesList from './Components/SalesList';
import EmployeeSales from './Components/EmployeeSales';
import { SaleForm } from "./Components/SaleForm";
import { CustomerForm } from "./Components/CustomerForm";
import { SalesPersonForm } from "./Components/SalesPersonForm";
import { ManufacturerList } from "./Components/ManufacturerList";
import { ManufacturerForm } from "./Components/ManufacturerForm";
import { VehicleModelList } from "./Components/VehicleModelList";
import { VehicleModelForm } from "./Components/VehicleModelForm";
import { AutomobileList } from "./Components/AutomobileList";
import { AutomobileForm } from "./Components/AutomobileForm";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path='/' element={<MainPage />} />

          <Route path="/" element={<MainPage />} />
          <Route path="sales" element={<SalesList />} />
          <Route path='employees/sales/' element={<EmployeeSales />} />
          <Route path='service'>
            <Route index element={<ServiceList />} />
            <Route path='new' element={<ServiceCreate />} />
            <Route path='search' element={<ServiceSearch />} />
            <Route path='new_technician' element={<TechnicianCreate />} />
            <Route path='list_technician' element={<TechnicianList />} />
          </Route>
          <Route path="manufacturers" element={<ManufacturerList />} />
          <Route path="manufacturers/new" element={<ManufacturerForm />} />
          <Route path="models" element={<VehicleModelList />} />
          <Route path="models/new" element={<VehicleModelForm />} />
          <Route path="automobiles" element={<AutomobileList />} />
          <Route path="automobiles/new" element={<AutomobileForm />} />

          <Route path="sales/new" element={<SaleForm />} />
          <Route path="customers/new" element={<CustomerForm />} />
          <Route path="employees/new" element={<SalesPersonForm />} />
        </Routes>
      </div>
    </BrowserRouter>

);
}


export default App;
