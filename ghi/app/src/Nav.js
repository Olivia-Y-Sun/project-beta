import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <ul className='dropdown'>
              <NavLink
                // className='btn btn-primary dropdown-toggle'
                className='nav-link active' aria-current="page"
                to='#'
                role='button'
                id='dropdownMenu'
                data-bs-toggle='dropdown'
                aria-haspopup='true'
                aria-expanded='false'
              >
                Services
              </NavLink>

            <div className='dropdown-menu' aria-labelledby='dropdownMenu'>
              <NavLink className='dropdown-item' to='/service/new_technician'>
                Create Technician
              </NavLink>
              <NavLink className='dropdown-item' to='/service/list_technician'>
                List Technician
              </NavLink>
              <li className='dropdown-divider' />
              <NavLink className='dropdown-item' to='/service'>
                Service List
              </NavLink>
              <NavLink className='dropdown-item' to='/service/new'>
                Create Service
              </NavLink>
              <NavLink className='dropdown-item' to='/service/search'>
                Service History
              </NavLink>
            </div>
            </ul>

            <li className="nav-item">
              <NavLink className="nav-link" to="/sales/">Sales List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/">Manufacturer List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models/">Vehicle Models List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/">Automobiles List</NavLink>
            </li>
          </ul>
          </div>
      </div>
    </nav>
  )
}

export default Nav;
