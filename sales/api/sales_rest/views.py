from django.shortcuts import render
from .models import SalesPerson, Sale, Customer, AutomobileVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .encoders import (
    AutomobileVODetailEncoder,
    SaleDetailEncoder,
    SalesPersonEncoder,
    CustomerDetailEncoder,
    CustomerListEncoder
)

# Create your views here.




@require_http_methods(["GET", "POST"])
def api_list_sales_person(request):
    if request.method == "GET":
        sales_people = SalesPerson.objects.all()
        return JsonResponse(
            {'sales_people': sales_people},
            encoder=SalesPersonEncoder
        )
    else:
        content = json.loads(request.body)

        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(sales_person,
            encoder=SalesPersonEncoder,
            safe=False
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_sales_person(request, pk):
    if request.method == "GET":
        sales_person = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = SalesPerson.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        SalesPerson.objects.filter(id=pk).update(**content)
        sales_person = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False
        )

@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {'customers': customers},
            encoder=CustomerListEncoder
        )
    else:
        content = json.loads(request.body)

        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_customer(request, pk):
    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        Customer.objects.filter(id=pk).update(**content)
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False
        )

@require_http_methods(["GET", "POST"])
def api_list_all_sales(request, sales_person_id=None):
    if request.method == "GET":
        if sales_person_id is not None:
            sales = Sale.objects.filter(sales_person=sales_person_id)
        else:
            sales = Sale.objects.all()
        return JsonResponse(
            {'sales': sales},
            encoder=SaleDetailEncoder
        )
    else:
        content = json.loads(request.body)

        customer_id = content["customer"]
        customer = Customer.objects.get(id=customer_id)
        content['customer'] = customer

        sales_person_id = content['sales_person']
        sales_person = SalesPerson.objects.get(id=sales_person_id)
        content["sales_person"] = sales_person

        try:
            automobile_href = content["automobile"]
            automobile = AutomobileVO.objects.get(import_href=automobile_href)
            content['automobile'] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Automobile ID"},
                status=400
            )
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_sale(request, pk):
    if request.method == "GET":
        sale = Sale.objects.get(id=pk)
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Sale.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        customer_id = content["customer"]
        customer = Customer.objects.get(id=customer_id)
        content['customer'] = customer

        sales_person_id = content['sales_person']
        sales_person = SalesPerson.objects.get(id=sales_person_id)
        content["sales_person"] = sales_person

        try:
            automobile_href = content["automobile"]
            automobile = AutomobileVO.objects.get(import_href=automobile_href)
            content['automobile'] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Automobile ID"},
                status=400
            )

        Sale.objects.filter(id=pk).update(**content)
        sale = Sale.objects.get(id=pk)
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False
        )

@require_http_methods("GET")
def api_unsold_automobiles(request):
    sold_vins = [sale.automobile.vin for sale in Sale.objects.all()]
    unsold = AutomobileVO.objects.exclude(vin__in=sold_vins)
    return JsonResponse(
        {'automobiles': unsold},
        encoder=AutomobileVODetailEncoder,
        safe=False
    )
